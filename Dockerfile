FROM node:12-alpine
COPY package.json .
COPY yarn.lock .
RUN yarn
COPY . .
CMD ["yarn", "start"]